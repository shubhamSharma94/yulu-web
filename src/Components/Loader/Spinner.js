import React from "react";
import "../../css/spinner.css";
export default function Spinner (props) {
  return  (
    <div className="lds-roller">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  )
}