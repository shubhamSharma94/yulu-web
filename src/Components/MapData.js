const mapData = `{
  "results_found": 1080,
  "results_start": 0,
  "results_shown": 20,
  "restaurants": [
    {
      "restaurant": {
        "R": {
          "res_id": 18627369
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18627369",
        "name": "Biergarten",
        "url": "https://www.zomato.com/bangalore/biergarten-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "4th B Cross, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9334267140",
          "longitude": "77.6143838838",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, North Indian, Chinese, European, BBQ, Finger Food, Asian",
        "average_cost_for_two": 2100,
        "price_range": 4,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/9/18627369/1e77b0549c7ca9fe29e7f162fd9d32a6.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.8",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "1945"
        },
        "photos_url": "https://www.zomato.com/bangalore/biergarten-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/biergarten-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/9/18627369/1e77b0549c7ca9fe29e7f162fd9d32a6.jpg",
        "medio_provider": 1,
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18627369",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/biergarten-koramangala-5th-block/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/biergarten-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18772094
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18772094",
        "name": "Bombay Adda",
        "url": "https://www.zomato.com/bangalore/bombay-adda-koramangala-7th-block-bangalore?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "6, Z1 Construction Building, 5th Floor, Next To Sapna Book House, 20th Main Road, Koramangala 7th Block, Bangalore",
          "locality": "Koramangala 7th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9348037208",
          "longitude": "77.6134464517",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Koramangala 7th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Modern Indian",
        "average_cost_for_two": 1100,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "zomato_events": [
          {
            "event": {
              "event_id": 351193,
              "friendly_start_date": "18 May",
              "friendly_end_date": "19 May",
              "friendly_timing_str": "Saturday, 18th May - Sunday, 19th May",
              "start_date": "2019-05-18",
              "end_date": "2019-05-19",
              "end_time": "01:00:43",
              "start_time": "19:00:43",
              "is_active": 1,
              "date_added": "2019-05-14 20:43:23",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/118/e3c3fcd741f9fe90e42d9faf0ef9f118_1557846803.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/118/e3c3fcd741f9fe90e42d9faf0ef9f118_1557846803.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "e3c3fcd741f9fe90e42d9faf0ef9f118",
                    "id": 473835,
                    "photo_id": 473835,
                    "uuid": 15578468024116,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Saturday Night Ft. Kaleem",
              "description": "Set Your Saturday Night Standards High,\nCuz we're gonna Make You Fly.\nThe only Decision to Make is to Drink by The Glass or A Bottle.\n\nSaturday Night\n\nFt. DJ Kaleem\n\n7pm | 18th May| Saturday| Bombay Adda Bangalore.\n\nBook Your Experience: 9606652222.\n\n#Club Rules Apply.\n#Rights Of Admission Reserved By Management.",
              "display_time": "07:00 pm - 01:00 am",
              "display_date": "18 May - 19 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          }
        ],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/4/18772094/a5aa4b6c459c7b5641d58c08233b5cb6.jpeg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.3",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "votes": "3397"
        },
        "photos_url": "https://www.zomato.com/bangalore/bombay-adda-koramangala-7th-block-bangalore/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/bombay-adda-koramangala-7th-block-bangalore/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/4/18772094/a5aa4b6c459c7b5641d58c08233b5cb6.jpeg?output-format=webp",
        "medio_provider": 1,
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18772094",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/bombay-adda-koramangala-7th-block-bangalore/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/bombay-adda-koramangala-7th-block-bangalore/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18605417
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18605417",
        "name": "Brooks and Bonds Brewery",
        "url": "https://www.zomato.com/bangalore/brooks-and-bonds-brewery-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "4, 100 Feet Road, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9342338357",
          "longitude": "77.6234309748",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, Mediterranean, North Indian, Chinese, Finger Food",
        "average_cost_for_two": 1600,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 1,
        "mezzo_provider": "ZOMATO_BOOK",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/7/18605417/0a6232e4bc3c07d9303c0df421287c82.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.3",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "votes": "4529"
        },
        "photos_url": "https://www.zomato.com/bangalore/brooks-and-bonds-brewery-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/brooks-and-bonds-brewery-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/7/18605417/0a6232e4bc3c07d9303c0df421287c82.jpg",
        "medio_provider": "",
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18605417",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/brooks-and-bonds-brewery-koramangala-5th-block/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/brooks-and-bonds-brewery-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 51040
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "51040",
        "name": "Truffles",
        "url": "https://www.zomato.com/bangalore/truffles-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "28, 4th 'B' Cross, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9334267140",
          "longitude": "77.6143838838",
          "zipcode": "0",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Cafe, American, Burger, Steak",
        "average_cost_for_two": 900,
        "price_range": 2,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/chains/8/51038/c312bf371d4e0aea2021a1fd824054ed.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.7",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "15222"
        },
        "photos_url": "https://www.zomato.com/bangalore/truffles-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/truffles-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/chains/8/51038/c312bf371d4e0aea2021a1fd824054ed.jpg?output-format=webp",
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/51040",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/bangalore/truffles-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 54162
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "54162",
        "name": "The Black Pearl",
        "url": "https://www.zomato.com/bangalore/the-black-pearl-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "105, 1st A Cross Road, Jyothi Nivas College Road, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9344024486",
          "longitude": "77.6161289960",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "North Indian, European, Mediterranean",
        "average_cost_for_two": 1400,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "zomato_events": [
          {
            "event": {
              "event_id": 319906,
              "friendly_start_date": "28 February",
              "friendly_end_date": "31 December",
              "friendly_timing_str": "Thursday, 28th February - Tuesday, 31st December",
              "start_date": "2019-02-28",
              "end_date": "2019-12-31",
              "end_time": "00:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-02-28 18:45:00",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/406/044b2f0cf1a6fb0611477a28fc9fb406_1553421281.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/406/044b2f0cf1a6fb0611477a28fc9fb406_1553421281.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "044b2f0cf1a6fb0611477a28fc9fb406",
                    "id": 444781,
                    "photo_id": 444781,
                    "uuid": 21271877920,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "WEEKLY OFFERS",
              "description": "* Weekly Offers \n   Mon-Sunday \n*Timing -12pm to 12am \n* T&C Apply ",
              "display_time": "12:00 pm - 12:00 am",
              "display_date": "28 February - 31 December",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 336921,
              "friendly_start_date": "08 April",
              "friendly_end_date": "31 March, 2020",
              "friendly_timing_str": "Monday, 8th April - Tuesday, 31st March",
              "start_date": "2019-04-08",
              "end_date": "2020-03-31",
              "end_time": "16:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-04-08 18:59:57",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/f93/c3f8ad86820067b04cd99322f8a55f93_1554730197.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/f93/c3f8ad86820067b04cd99322f8a55f93_1554730197.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "c3f8ad86820067b04cd99322f8a55f93",
                    "id": 453946,
                    "photo_id": 453946,
                    "uuid": 1554730181364850,
                    "type": "NORMAL"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/327/4cf84c9544be624bcd1c9fab93448327_1554730197.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/327/4cf84c9544be624bcd1c9fab93448327_1554730197.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 1,
                    "md5sum": "4cf84c9544be624bcd1c9fab93448327",
                    "id": 453947,
                    "photo_id": 453947,
                    "uuid": 1554730181434307,
                    "type": "NORMAL"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/278/bb492f20db1d931461d6fd06d766f278_1554730197.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/278/bb492f20db1d931461d6fd06d766f278_1554730197.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 2,
                    "md5sum": "bb492f20db1d931461d6fd06d766f278",
                    "id": 453948,
                    "photo_id": 453948,
                    "uuid": 1554730181492558,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Unlimited Alcohol Offers at The Black Pearl",
              "description": "Add 349 to your Buffet and get Unlimited Brewed Beer for Lunch - 90 mins\nAdd 599 to your Buffet and get Unlimited IML & Brewed Beer for Lunch - 90 mins\nAdd 799 to your Buffet and get Unlimited IML, IMFL & Brewed Beer for Lunch - 90 mins\n",
              "display_time": "12:00 pm - 04:00 pm",
              "display_date": "08 April - 31 March",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 336924,
              "friendly_start_date": "08 April",
              "friendly_end_date": "31 March, 2020",
              "friendly_timing_str": "Monday, 8th April - Tuesday, 31st March",
              "start_date": "2019-04-08",
              "end_date": "2020-03-31",
              "end_time": "23:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-04-08 19:04:01",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/2d6/138ca755a6df9ffa0e24ab4f6a4c02d6_1554730441.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/2d6/138ca755a6df9ffa0e24ab4f6a4c02d6_1554730441.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "138ca755a6df9ffa0e24ab4f6a4c02d6",
                    "id": 453953,
                    "photo_id": 453953,
                    "uuid": 1554730432189275,
                    "type": "NORMAL"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/bae/d892991dfb844b37eff81f77576cabae_1554730442.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/bae/d892991dfb844b37eff81f77576cabae_1554730442.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 1,
                    "md5sum": "d892991dfb844b37eff81f77576cabae",
                    "id": 453954,
                    "photo_id": 453954,
                    "uuid": 1554730432427424,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Every 11 pax get 1 free buffet, Every 20 pax get 2 free buffet",
              "description": "Every 11 pax get 1 free buffet - Mon & Tuesday Lunch & Dinner\nEvery 20 pax get 2 free buffet - Wed & Thursday Lunch and Dinner \n",
              "display_time": "12:00 pm - 11:00 pm",
              "display_date": "08 April - 31 March",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 336929,
              "friendly_start_date": "08 April",
              "friendly_end_date": "31 March, 2020",
              "friendly_timing_str": "Monday, 8th April - Tuesday, 31st March",
              "start_date": "2019-04-08",
              "end_date": "2020-03-31",
              "end_time": "23:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-04-08 19:07:05",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/7dd/f8ed5d2cae323b42cfc3f977d04e97dd_1554730625.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/7dd/f8ed5d2cae323b42cfc3f977d04e97dd_1554730625.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "f8ed5d2cae323b42cfc3f977d04e97dd",
                    "id": 453959,
                    "photo_id": 453959,
                    "uuid": 1554730556662810,
                    "type": "NORMAL"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/db2/4087e87dbfe0545c0c11f9c1ebbfbdb2_1554730625.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/db2/4087e87dbfe0545c0c11f9c1ebbfbdb2_1554730625.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 1,
                    "md5sum": "4087e87dbfe0545c0c11f9c1ebbfbdb2",
                    "id": 453960,
                    "photo_id": 453960,
                    "uuid": 1554730556905562,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Early Bird Offer & Full Bottle Offer",
              "description": "Enjoy 10% off from Monday to Sunday 6pm-7pm \n\nEnjoy 20% off on any full bottle of alcohol all days\n\n",
              "display_time": "12:00 pm - 11:00 pm",
              "display_date": "08 April - 31 March",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 336932,
              "friendly_start_date": "08 April",
              "friendly_end_date": "31 March, 2020",
              "friendly_timing_str": "Monday, 8th April - Tuesday, 31st March",
              "start_date": "2019-04-08",
              "end_date": "2020-03-31",
              "end_time": "21:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-04-08 19:08:34",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/359/40bdc0c46472fe6eeb756f47fce01359_1554730726.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/359/40bdc0c46472fe6eeb756f47fce01359_1554730726.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "40bdc0c46472fe6eeb756f47fce01359",
                    "id": 453966,
                    "photo_id": 453966,
                    "uuid": 1554730723168960,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Sunday Specials at The Black Pearl",
              "description": "A complimentary Glass of Cocktail/Mocktail or Brewed Beer with Sunday Lunch!",
              "display_time": "12:00 pm - 09:00 pm",
              "display_date": "08 April - 31 March",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 348630,
              "friendly_start_date": "08 May",
              "friendly_end_date": "30 June",
              "friendly_timing_str": "Wednesday, 8th May - Sunday, 30th June",
              "start_date": "2019-05-08",
              "end_date": "2019-06-30",
              "end_time": "22:00:00",
              "start_time": "21:00:00",
              "is_active": 1,
              "date_added": "2019-05-08 22:05:54",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/462/63dda133cef91a8c5fe87f4576f79462_1557333450.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/462/63dda133cef91a8c5fe87f4576f79462_1557333450.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "63dda133cef91a8c5fe87f4576f79462",
                    "id": 470372,
                    "photo_id": 470372,
                    "uuid": 33421207910,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Click, Share & Win \"Selfie Contest",
              "description": "Ahoy Matey's!\nClick, Share & Win \"SELFIE CONTEST\"\nPost your best selfie taken @ the black pearl, Koramangala and post it on your FB page to win a surprise gift",
              "display_time": "09:00 pm - 10:00 pm",
              "display_date": "08 May - 30 June",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          }
        ],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/2/54162/60a95e08f9b514d2b3afae3579f7caf1.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.7",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "11183"
        },
        "photos_url": "https://www.zomato.com/bangalore/the-black-pearl-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/the-black-pearl-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/2/54162/60a95e08f9b514d2b3afae3579f7caf1.jpg",
        "medio_provider": 1,
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/54162",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/the-black-pearl-koramangala-5th-block/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/the-black-pearl-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18709627
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18709627",
        "name": "Simon Says Brew Works",
        "url": "https://www.zomato.com/bangalore/simon-says-brew-works-koramangala-5th-block-bangalore?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "105, 3rd Floor, 1st A Cross Road, Jyothi Nivas College Road, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9344024486",
          "longitude": "77.6161289960",
          "zipcode": "560095",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, American, Italian, Modern Indian, Pizza, Burger",
        "average_cost_for_two": 1400,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "zomato_events": [
          {
            "event": {
              "event_id": 297581,
              "friendly_start_date": "13 January",
              "friendly_end_date": "28 July",
              "friendly_timing_str": "Sunday, 13th January - Sunday, 28th July",
              "start_date": "2019-01-13",
              "end_date": "2019-07-28",
              "end_time": "23:00:00",
              "start_time": "20:00:00",
              "is_active": 1,
              "date_added": "2019-01-09 18:30:57",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/547/bea82ff7d15398b48a4f3aee2acc4547_1557490968.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/547/bea82ff7d15398b48a4f3aee2acc4547_1557490968.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "bea82ff7d15398b48a4f3aee2acc4547",
                    "id": 471485,
                    "photo_id": 471485,
                    "uuid": 90857370896,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "BOLLYWOOD KARAOKE & FREE DRINKS FOR LADIES",
              "description": "Bollywood Karaoke @ Simon Says Brew Works,Every Sunday,8pm Onwards with KJ Prithvi Raj Followed by DJ Snjy. FREE Drinks for LADIES :) *T&C Apply ",
              "display_time": "08:00 pm - 11:00 pm",
              "display_date": "13 January - 28 July",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Ladies Night",
                  "color": "#0AAC9B"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 316807,
              "friendly_start_date": "01 May",
              "friendly_end_date": "31 May",
              "friendly_timing_str": "Wednesday, 1st May - Friday, 31st May",
              "start_date": "2019-05-01",
              "end_date": "2019-05-31",
              "end_time": "17:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-02-21 13:14:22",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/d52/923136c47356a4524ad049223b242d52_1557567235.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/d52/923136c47356a4524ad049223b242d52_1557567235.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "923136c47356a4524ad049223b242d52",
                    "id": 471930,
                    "photo_id": 471930,
                    "uuid": 67087890473,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "ULTIMATE LUNCH MENU @249",
              "description": "ULTIMATE LUNCH MENU ALONG WITH UNLIMITED FRESHLY BREWED BEER",
              "display_time": "12:00 pm - 05:00 pm",
              "display_date": "01 May - 31 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Special Menu",
                  "color": "#F96C6A"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 330592,
              "friendly_start_date": "01 May",
              "friendly_end_date": "31 May",
              "friendly_timing_str": "Wednesday, 1st May - Friday, 31st May",
              "start_date": "2019-05-01",
              "end_date": "2019-05-31",
              "end_time": "00:30:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-03-25 16:04:05",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/625/99bb1e255821323769b97107ed7bc625_1554463669.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/625/99bb1e255821323769b97107ed7bc625_1554463669.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "99bb1e255821323769b97107ed7bc625",
                    "id": 452452,
                    "photo_id": 452452,
                    "uuid": 63624989855,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": " HAPPY HOURS 1+1 ON ALL LIQUORS FULL DAY",
              "description": "MONDAY AND TUESDAY FULL DAY HAPPY HOURS 1+1 ON ALL LIQUORS (INCLUDING FRESHLY BREWED BEER)",
              "display_time": "12:00 pm - 12:30 am",
              "display_date": "01 May - 31 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Happy Hours",
                  "color": "#E23744"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 331692,
              "friendly_start_date": "01 May",
              "friendly_end_date": "31 May",
              "friendly_timing_str": "Wednesday, 1st May - Friday, 31st May",
              "start_date": "2019-05-01",
              "end_date": "2019-05-31",
              "end_time": "00:30:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-03-27 13:16:26",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/923/edd374273fe9b39f032739c1d7e43923_1554887124.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/923/edd374273fe9b39f032739c1d7e43923_1554887124.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "edd374273fe9b39f032739c1d7e43923",
                    "id": 455062,
                    "photo_id": 455062,
                    "uuid": 87096085474,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "WEDNESDAY LADIES NIGHT WITH DJ ARPITAA ",
              "description": "ENJOY LADIES NIGHT WITH UNLIMITED COCKTAILS, SHOTS AND BEER WITH DJ ARPITAA 8 PM ONWARDS EVERY WEDNESDAY. T&C APPLY.",
              "display_time": "12:00 pm - 12:30 am",
              "display_date": "01 May - 31 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Ladies Night",
                  "color": "#0AAC9B"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 332016,
              "friendly_start_date": "01 May",
              "friendly_end_date": "31 May",
              "friendly_timing_str": "Wednesday, 1st May - Friday, 31st May",
              "start_date": "2019-05-01",
              "end_date": "2019-05-31",
              "end_time": "00:30:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-03-27 23:44:51",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/353/b262f413eae9ff0f8c35a8aa7bc1a353_1555916698.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/353/b262f413eae9ff0f8c35a8aa7bc1a353_1555916698.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "b262f413eae9ff0f8c35a8aa7bc1a353",
                    "id": 460965,
                    "photo_id": 460965,
                    "uuid": 16678446261,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "SEASONAL FRUITS BREWED BEER",
              "description": "ENJOY SEASONAL FRUITS BREWED BEER (MANGO, PINEAPPLE, POMEGRANATE)",
              "display_time": "12:00 pm - 12:30 am",
              "display_date": "01 May - 31 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 332132,
              "friendly_start_date": "01 May",
              "friendly_end_date": "31 May",
              "friendly_timing_str": "Wednesday, 1st May - Friday, 31st May",
              "start_date": "2019-05-01",
              "end_date": "2019-05-31",
              "end_time": "12:30:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-03-28 13:02:09",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/884/c176b4703ee42740aa4d30a978d1b884_1557567040.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/884/c176b4703ee42740aa4d30a978d1b884_1557567040.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "c176b4703ee42740aa4d30a978d1b884",
                    "id": 471927,
                    "photo_id": 471927,
                    "uuid": 67021218567,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "WEEKLY OFFERS AND EVENTS",
              "description": "WEEKLY OFFERS AND EVENTS DETAILS",
              "display_time": "12:00 pm - 12:30 pm",
              "display_date": "01 May - 31 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Promos",
                  "color": "#F96C6A"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 315874,
              "friendly_start_date": "01 May",
              "friendly_end_date": "31 May",
              "friendly_timing_str": "Wednesday, 1st May - Friday, 31st May",
              "start_date": "2019-05-01",
              "end_date": "2019-05-31",
              "end_time": "01:00:00",
              "start_time": "20:00:00",
              "is_active": 1,
              "date_added": "2019-02-19 15:21:42",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/278/bb1e372263762c810a7c220d732bd278_1550569902.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/278/bb1e372263762c810a7c220d732bd278_1550569902.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "bb1e372263762c810a7c220d732bd278",
                    "id": 425793,
                    "photo_id": 425793,
                    "uuid": 69740093623,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Bollywood Night with DJ Oppozit",
              "description": "LADIES GET FREE DRINKS , DANCE TO THE TUNES OF DJ OPPOZIT BANGING SOME OF THE HOTTEST HIP HOP & BOLLYWOOD TRACKS.\n*T&C APPLY ",
              "display_time": "08:00 pm - 01:00 am",
              "display_date": "01 May - 31 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Bollywood Night",
                  "color": "#6454B8"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 348978,
              "friendly_start_date": "09 May",
              "friendly_end_date": "30 June",
              "friendly_timing_str": "Thursday, 9th May - Sunday, 30th June",
              "start_date": "2019-05-09",
              "end_date": "2019-06-30",
              "end_time": "18:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-05-09 16:49:12",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/e96/417df9d8a5b52c823aa91d557ed4ee96_1557400764.png",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/e96/417df9d8a5b52c823aa91d557ed4ee96_1557400764.png?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "417df9d8a5b52c823aa91d557ed4ee96",
                    "id": 470814,
                    "photo_id": 470814,
                    "uuid": 1557400761712876,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Click Share and Win!",
              "description": "Selfie Contest!",
              "display_time": "12:00 pm - 06:00 pm",
              "display_date": "09 May - 30 June",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Promos",
                  "color": "#F96C6A"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 348990,
              "friendly_start_date": "09 May",
              "friendly_end_date": "30 June",
              "friendly_timing_str": "Thursday, 9th May - Sunday, 30th June",
              "start_date": "2019-05-09",
              "end_date": "2019-06-30",
              "end_time": "21:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-05-09 17:01:10",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/be4/3bb24088c89df16a116d403e9ad8dbe4_1557401470.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/be4/3bb24088c89df16a116d403e9ad8dbe4_1557401470.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "3bb24088c89df16a116d403e9ad8dbe4",
                    "id": 470833,
                    "photo_id": 470833,
                    "uuid": 921522318,
                    "type": "FEATURED"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/ba0/c036f1ca48fc010d6aa02d4ed1bfdba0_1557401470.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/ba0/c036f1ca48fc010d6aa02d4ed1bfdba0_1557401470.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 1,
                    "md5sum": "c036f1ca48fc010d6aa02d4ed1bfdba0",
                    "id": 470834,
                    "photo_id": 470834,
                    "uuid": 921523513,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "UNLIMITED PACKAGES @JUST 999 ONWARDS",
              "description": "Unlimited liquors alongwith unlimited food ",
              "display_time": "12:00 pm - 09:00 pm",
              "display_date": "09 May - 30 June",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Promos",
                  "color": "#F96C6A"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          }
        ],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/7/18709627/c92d1912e4d119fc5b2ea1b5517b4be4.jpeg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.4",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "votes": "2672"
        },
        "photos_url": "https://www.zomato.com/bangalore/simon-says-brew-works-koramangala-5th-block-bangalore/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/simon-says-brew-works-koramangala-5th-block-bangalore/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/7/18709627/c92d1912e4d119fc5b2ea1b5517b4be4.jpeg?output-format=webp",
        "medio_provider": 1,
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18709627",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/simon-says-brew-works-koramangala-5th-block-bangalore/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/simon-says-brew-works-koramangala-5th-block-bangalore/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18537585
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18537585",
        "name": "The Reservoire",
        "url": "https://www.zomato.com/bangalore/the-reservoire-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "17th Main Road, JNC Road, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9335142885",
          "longitude": "77.6222622022",
          "zipcode": "560095",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, North Indian, Chinese, American",
        "average_cost_for_two": 1300,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "zomato_events": [
          {
            "event": {
              "event_id": 350903,
              "friendly_start_date": "14 May",
              "friendly_end_date": "14 June",
              "friendly_timing_str": "Tuesday, 14th May - Friday, 14th June",
              "start_date": "2019-05-14",
              "end_date": "2019-06-14",
              "end_time": "18:00:26",
              "start_time": "11:00:26",
              "is_active": 1,
              "date_added": "2019-05-14 13:39:29",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/92c/558b523c78baa78dbccfb6c73eacb92c_1554826980.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/92c/558b523c78baa78dbccfb6c73eacb92c_1554826980.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "558b523c78baa78dbccfb6c73eacb92c",
                    "id": 473527,
                    "photo_id": 473527,
                    "uuid": 15578213696638,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Happy Hours @ Reservoire",
              "description": "The Happy hazy hours is reasons to celebrate on the weekdays!\n\nCocktails, Beer,Hard liquor and a lot more options to indulge into!\n\nMonday- Tuesday: 11:00- 20:00\nWednesday-Friday: 11:00:18:00\n",
              "display_time": "11:00 am - 06:00 pm",
              "display_date": "14 May - 14 June",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Happy Hours",
                  "color": "#E23744"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 351196,
              "friendly_start_date": "15 May",
              "friendly_end_date": "26 June",
              "friendly_timing_str": "Wednesday, 15th May - Wednesday, 26th June",
              "start_date": "2019-05-15",
              "end_date": "2019-06-26",
              "end_time": "22:30:00",
              "start_time": "20:30:00",
              "is_active": 1,
              "date_added": "2019-05-14 21:00:29",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/c6e/633a50277c93865ec6c6d0338b175c6e_1557847870.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/c6e/633a50277c93865ec6c6d0338b175c6e_1557847870.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "633a50277c93865ec6c6d0338b175c6e",
                    "id": 473839,
                    "photo_id": 473839,
                    "uuid": 1557847868513762,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Bollywood Live Music",
              "description": "With the finest cocktails in town, enjoy live music with Arijeet Bhattacharya every Wednesday from 8:30 pm to 10:30 pm.",
              "display_time": "08:30 pm - 10:30 pm",
              "display_date": "15 May - 26 June",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 352267,
              "friendly_start_date": "21 May",
              "friendly_end_date": "21 May",
              "friendly_timing_str": "Tuesday, 21st May",
              "start_date": "2019-05-21",
              "end_date": "2019-05-21",
              "end_time": "23:55:00",
              "start_time": "19:30:00",
              "is_active": 1,
              "date_added": "2019-05-17 17:34:03",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/1cb/9cc9ae21a06e2298ccaded48c6c8c1cb_1558094643.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/1cb/9cc9ae21a06e2298ccaded48c6c8c1cb_1558094643.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "9cc9ae21a06e2298ccaded48c6c8c1cb",
                    "id": 475422,
                    "photo_id": 475422,
                    "uuid": 1558094623508379,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Rock the night with Vinit Udayavar",
              "description": "Experience the finest cocktails by Vinit Udayavar, Brand Ambassador of Jack Daniels only at The Reservoire on 21st May, Tuesday 7:30 pm onwards",
              "display_time": "07:30 pm - 11:55 pm",
              "display_date": "21 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          }
        ],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/5/18537585/a6ddff34ec4460d510da5de7bce96cb6.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.5",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "2854"
        },
        "photos_url": "https://www.zomato.com/bangalore/the-reservoire-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/the-reservoire-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/5/18537585/a6ddff34ec4460d510da5de7bce96cb6.jpg?output-format=webp",
        "medio_provider": 1,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18537585",
        "order_url": "https://www.zomato.com/bangalore/the-reservoire-koramangala-5th-block/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/the-reservoire-koramangala-5th-block/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/the-reservoire-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18727906
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18727906",
        "name": "XOOX Brewmill",
        "url": "https://www.zomato.com/bangalore/xoox-brewmill-koramangala-5th-block-bangalore?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "8, Koramanagala Industrial Layout, Near HDFC Bank, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9355510000",
          "longitude": "77.6150600000",
          "zipcode": "560095",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Modern Indian, European, Asian",
        "average_cost_for_two": 2000,
        "price_range": 4,
        "currency": "Rs.",
        "offers": [],
        "zomato_events": [
          {
            "event": {
              "event_id": 344611,
              "friendly_start_date": "17 May",
              "friendly_end_date": "31 May",
              "friendly_timing_str": "Friday, 17th May - Friday, 31st May",
              "start_date": "2019-05-17",
              "end_date": "2019-05-31",
              "end_time": "18:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-04-30 14:57:33",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/db0/3ee5148d38ab1f627bb30b8ebf8e3db0_1556616453.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/db0/3ee5148d38ab1f627bb30b8ebf8e3db0_1556616453.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "3ee5148d38ab1f627bb30b8ebf8e3db0",
                    "id": 464643,
                    "photo_id": 464643,
                    "uuid": 16380547263,
                    "type": "FEATURED"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/e67/5aa70101b6bcb8f8d3e89435a3e37e67_1556616454.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/e67/5aa70101b6bcb8f8d3e89435a3e37e67_1556616454.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 1,
                    "md5sum": "5aa70101b6bcb8f8d3e89435a3e37e67",
                    "id": 464644,
                    "photo_id": 464644,
                    "uuid": 16380547935,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "\"Happy Hours By The Bottle\"",
              "description": "Now Enjoy Special Rates on all Bottle Purchases every Monday - Friday between 12 Noon until 6 pm throughout May. Table reservations recommended. Book Now.",
              "display_time": "12:00 pm - 06:00 pm",
              "display_date": "17 May - 31 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Happy Hours",
                  "color": "#E23744"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 344613,
              "friendly_start_date": "20 May",
              "friendly_end_date": "31 May",
              "friendly_timing_str": "Monday, 20th May - Friday, 31st May",
              "start_date": "2019-05-20",
              "end_date": "2019-05-31",
              "end_time": "16:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-04-30 15:02:05",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/9e0/06e5fdfd3194d68e27a5bba8c9a609e0_1556616725.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/9e0/06e5fdfd3194d68e27a5bba8c9a609e0_1556616725.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "06e5fdfd3194d68e27a5bba8c9a609e0",
                    "id": 464647,
                    "photo_id": 464647,
                    "uuid": 16706267641,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "\"Pizza Luncheon\"",
              "description": "This May, drop by the Mill for exciting combos :\n\n15\" Veg Pizza + 330 ml Craft Beer = INR 444\n15\" Non Veg Pizza + 330 ml Craft Beer = INR 555",
              "display_time": "12:00 pm - 04:00 pm",
              "display_date": "20 May - 31 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          }
        ],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/6/18727906/b2f9919687e6c87cc737323519de9f3f.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.6",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "1911"
        },
        "photos_url": "https://www.zomato.com/bangalore/xoox-brewmill-koramangala-5th-block-bangalore/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/xoox-brewmill-koramangala-5th-block-bangalore/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/6/18727906/b2f9919687e6c87cc737323519de9f3f.jpg",
        "medio_provider": 1,
        "has_online_delivery": 1,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18727906",
        "is_table_reservation_supported": 1,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/bangalore/xoox-brewmill-koramangala-5th-block-bangalore/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18843699
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18843699",
        "name": "The Bier Library",
        "url": "https://www.zomato.com/bangalore/the-bier-library-koramangala-6th-block-bangalore?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "14, Patel Narayana Reddy Layout, 80 Feet Main Road, Koramangala 6th Block, Bangalore",
          "locality": "Koramangala 6th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9390760000",
          "longitude": "77.6260650000",
          "zipcode": "560095",
          "country_id": 1,
          "locality_verbose": "Koramangala 6th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, North Indian, Pizza, BBQ",
        "average_cost_for_two": 2000,
        "price_range": 4,
        "currency": "Rs.",
        "offers": [],
        "zomato_events": [
          {
            "event": {
              "event_id": 324418,
              "friendly_start_date": "18 March",
              "friendly_end_date": "30 June",
              "friendly_timing_str": "Monday, 18th March - Sunday, 30th June",
              "start_date": "2019-03-18",
              "end_date": "2019-06-30",
              "end_time": "19:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-03-11 10:32:08",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/851/946d35718d6b4fcaf5862eceeeb57851_1552890652.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/851/946d35718d6b4fcaf5862eceeeb57851_1552890652.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "946d35718d6b4fcaf5862eceeeb57851",
                    "id": 441251,
                    "photo_id": 441251,
                    "uuid": 1552890648075519,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Happy Hours: Buy 2 craft beers and Get the 3rd free!",
              "description": "Bring your friends, bring your family, bring yourself, we've got an offer you cannot resist. Buy 2 beers and get the 3rd free*, every Monday to Thursday between 12 - 7 PM at The Bier Library.\r\n\r\n* Two offers cannot be clubbed together. Offer not applicable on holidays. Offer valid only from Monday-Thursday.",
              "display_time": "12:00 pm - 07:00 pm",
              "display_date": "18 March - 30 June",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Happy Hours",
                  "color": "#E23744"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 316230,
              "friendly_start_date": "01 May",
              "friendly_end_date": "30 June",
              "friendly_timing_str": "Wednesday, 1st May - Sunday, 30th June",
              "start_date": "2019-05-01",
              "end_date": "2019-06-30",
              "end_time": "19:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-02-20 10:25:41",
              "photos": [],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Corporate Deals & Offers : 15% discount on overall bill",
              "description": "Come, Drink, Flash your corporate ID card and get 15% discount on overall bill, Monday to Friday 12 - 7 pm!\r\n\r\nOffer not applicable on public holidays.",
              "display_time": "12:00 pm - 07:00 pm",
              "display_date": "01 May - 30 June",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Corporate Discounts",
                  "color": "#E23744"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 346149,
              "friendly_start_date": "03 May",
              "friendly_end_date": "30 June",
              "friendly_timing_str": "Friday, 3rd May - Sunday, 30th June",
              "start_date": "2019-05-03",
              "end_date": "2019-06-30",
              "end_time": "16:00:00",
              "start_time": "13:00:00",
              "is_active": 1,
              "date_added": "2019-05-03 18:09:00",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/651/25066f3aecf04f061126e34fdcc7a651_1556890928.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/651/25066f3aecf04f061126e34fdcc7a651_1556890928.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "25066f3aecf04f061126e34fdcc7a651",
                    "id": 466665,
                    "photo_id": 466665,
                    "uuid": 1556890925256753,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Corporate Log Off Lunch",
              "description": "Log off and come lunch with us!\nOur corporate lunch packages are ideal for quick escape and delicious bite outside the office!\nSo reward your hard working self and drop by The Bier Library!\nMonday to Friday 1 to 4 pm!\nVeg 349/-\nNon Veg 399/-",
              "display_time": "01:00 pm - 04:00 pm",
              "display_date": "03 May - 30 June",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 351290,
              "friendly_start_date": "17 May",
              "friendly_end_date": "27 May",
              "friendly_timing_str": "Friday, 17th May - Monday, 27th May",
              "start_date": "2019-05-17",
              "end_date": "2019-05-27",
              "end_time": "23:55:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-05-15 11:40:42",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/35b/8f79963703e35bbe0f58a894ca4ac35b_1557900642.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/35b/8f79963703e35bbe0f58a894ca4ac35b_1557900642.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "8f79963703e35bbe0f58a894ca4ac35b",
                    "id": 474012,
                    "photo_id": 474012,
                    "uuid": 1557900592803310,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "TBL's Burger Festival : Between The Buns",
              "description": "TBL is launching the Big Burger Festival.\nBest Burgers with craft beer and wine.\nCome Visit us for a Yummiest Experience.\n",
              "display_time": "12:00 pm - 11:55 pm",
              "display_date": "17 May - 27 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Food Festival",
                  "color": "#F96C6A"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 351293,
              "friendly_start_date": "17 May",
              "friendly_end_date": "27 May",
              "friendly_timing_str": "Friday, 17th May - Monday, 27th May",
              "start_date": "2019-05-17",
              "end_date": "2019-05-27",
              "end_time": "23:55:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-05-15 11:44:43",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/35b/8f79963703e35bbe0f58a894ca4ac35b_1557900903.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/35b/8f79963703e35bbe0f58a894ca4ac35b_1557900903.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "8f79963703e35bbe0f58a894ca4ac35b",
                    "id": 474016,
                    "photo_id": 474016,
                    "uuid": 1557900901676115,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "TBL's Burger Festival: Between The Buns",
              "description": "TBL is launching its first ever Burger Fest.\nBringing the world class burger with craft beer & wines.\nVisit us for the yummiest experience.\nBook Now!",
              "display_time": "12:00 pm - 11:55 pm",
              "display_date": "17 May - 27 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Food Festival",
                  "color": "#F96C6A"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          }
        ],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/9/18843699/87bf822f84f8cf65aa4b7095099ef33b.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.4",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "votes": "870"
        },
        "photos_url": "https://www.zomato.com/bangalore/the-bier-library-koramangala-6th-block-bangalore/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/the-bier-library-koramangala-6th-block-bangalore/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/9/18843699/87bf822f84f8cf65aa4b7095099ef33b.jpg",
        "medio_provider": 1,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18843699",
        "order_url": "https://www.zomato.com/bangalore/the-bier-library-koramangala-6th-block-bangalore/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/the-bier-library-koramangala-6th-block-bangalore/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/the-bier-library-koramangala-6th-block-bangalore/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18996772
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18996772",
        "name": "The Hobbit Cafe",
        "url": "https://www.zomato.com/bangalore/the-hobbit-cafe-koramangala-5th-block-bangalore?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "12, 1st Main Road, Industrial Layout, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9351240000",
          "longitude": "77.6152590000",
          "zipcode": "560034",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, Burger, Pizza, Asian",
        "average_cost_for_two": 700,
        "price_range": 2,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/2/18996772/4cc67057bc90770ca30bd9949ebcb7e8.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.6",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "228"
        },
        "photos_url": "https://www.zomato.com/bangalore/the-hobbit-cafe-koramangala-5th-block-bangalore/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/the-hobbit-cafe-koramangala-5th-block-bangalore/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/2/18996772/4cc67057bc90770ca30bd9949ebcb7e8.jpg?output-format=webp",
        "medio_provider": 1,
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18996772",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/the-hobbit-cafe-koramangala-5th-block-bangalore/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/the-hobbit-cafe-koramangala-5th-block-bangalore/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 50691
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "50691",
        "name": "Meghana Foods",
        "url": "https://www.zomato.com/bangalore/meghana-foods-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "124, Near Jyothi Nivas College, 1st Cross, KHB Colony, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9344913298",
          "longitude": "77.6162040979",
          "zipcode": "0",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Biryani, Andhra, North Indian, Seafood",
        "average_cost_for_two": 600,
        "price_range": 2,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/chains/1/50691/671daf683a6918f20b705b34c7a8aeb0.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.4",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "votes": "7442"
        },
        "photos_url": "https://www.zomato.com/bangalore/meghana-foods-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/meghana-foods-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/chains/1/50691/671daf683a6918f20b705b34c7a8aeb0.jpg",
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/50691",
        "order_url": "https://www.zomato.com/bangalore/meghana-foods-koramangala-5th-block/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/bangalore/meghana-foods-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 61381
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "61381",
        "name": "Kota Kachori",
        "url": "https://www.zomato.com/bangalore/kota-kachori-koramangala-6th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "635, 100 Feet Road, Koramangala 6th Block, Bangalore",
          "locality": "Koramangala 6th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9361486969",
          "longitude": "77.6254094392",
          "zipcode": "560095",
          "country_id": 1,
          "locality_verbose": "Koramangala 6th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Street Food, Desserts, North Indian, Mithai, Rajasthani",
        "average_cost_for_two": 400,
        "price_range": 1,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/1/61381/821b22a21202ee0efbb227151d997bc4.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.3",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "votes": "3810"
        },
        "photos_url": "https://www.zomato.com/bangalore/kota-kachori-koramangala-6th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/kota-kachori-koramangala-6th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/1/61381/821b22a21202ee0efbb227151d997bc4.jpg",
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/61381",
        "order_url": "https://www.zomato.com/bangalore/kota-kachori-koramangala-6th-block/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/bangalore/kota-kachori-koramangala-6th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18682623
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18682623",
        "name": "Roundup Cafe",
        "url": "https://www.zomato.com/bangalore/roundup-cafe-koramangala-5th-block-bangalore?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "20, Sai Nivas, 1st A Main Road, KHB Colony, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9377510000",
          "longitude": "77.6175650000",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Cafe, Continental, Beverages",
        "average_cost_for_two": 800,
        "price_range": 2,
        "currency": "Rs.",
        "offers": [],
        "zomato_events": [
          {
            "event": {
              "event_id": 329802,
              "friendly_start_date": "23 March",
              "friendly_end_date": "31 May",
              "friendly_timing_str": "Saturday, 23rd March - Friday, 31st May",
              "start_date": "2019-03-23",
              "end_date": "2019-05-31",
              "end_time": "23:30:00",
              "start_time": "16:00:00",
              "is_active": 1,
              "date_added": "2019-03-23 13:46:16",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/43d/8fcfc50624c1d76c646c182169ece43d_1554816780.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/43d/8fcfc50624c1d76c646c182169ece43d_1554816780.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "8fcfc50624c1d76c646c182169ece43d",
                    "id": 454641,
                    "photo_id": 454641,
                    "uuid": 1668256333,
                    "type": "FEATURED"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/699/18253346285118f5270265865abfa699_1554816780.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/699/18253346285118f5270265865abfa699_1554816780.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 1,
                    "md5sum": "18253346285118f5270265865abfa699",
                    "id": 454642,
                    "photo_id": 454642,
                    "uuid": 16682563342,
                    "type": "NORMAL"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/22a/22068a3c3cb13a388629e7b4201cd22a_1554816781.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/22a/22068a3c3cb13a388629e7b4201cd22a_1554816781.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 2,
                    "md5sum": "22068a3c3cb13a388629e7b4201cd22a",
                    "id": 454644,
                    "photo_id": 454644,
                    "uuid": 16775364635,
                    "type": "NORMAL"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/fdb/f94aa49c2e05be8e98c4de30eaab7fdb_1553329059.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/fdb/f94aa49c2e05be8e98c4de30eaab7fdb_1553329059.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 3,
                    "md5sum": "f94aa49c2e05be8e98c4de30eaab7fdb",
                    "id": 444272,
                    "photo_id": 444272,
                    "uuid": 2904088742,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Open Air Rooftop Match Screening - FREE",
              "description": "Now watch IPL at Roundup's rooftop on big screen with your gang and enjoy magical mists android smacking continental cuisine. Full HD screening, 10 feet x 6 feet screen. No charge.\n\nMagical mists @299. ",
              "display_time": "04:00 pm - 11:30 pm",
              "display_date": "23 March - 31 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Live Cricket Screening 2019",
                  "color": "#0DA314"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          }
        ],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/3/18682623/17f5e44edcc99472717ea005deb8014b.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.2",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "votes": "798"
        },
        "photos_url": "https://www.zomato.com/bangalore/roundup-cafe-koramangala-5th-block-bangalore/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/roundup-cafe-koramangala-5th-block-bangalore/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/3/18682623/17f5e44edcc99472717ea005deb8014b.jpg?output-format=webp",
        "medio_provider": 1,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18682623",
        "order_url": "https://www.zomato.com/bangalore/roundup-cafe-koramangala-5th-block-bangalore/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/roundup-cafe-koramangala-5th-block-bangalore/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/roundup-cafe-koramangala-5th-block-bangalore/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18221544
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18221544",
        "name": "Asia Kitchen By Mainland China",
        "url": "https://www.zomato.com/bangalore/asia-kitchen-by-mainland-china-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "136, Ground Floor, 1st Cross, 5th Block, Jyoti Niwas College Road, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9335796425",
          "longitude": "77.6214753091",
          "zipcode": "560095",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Asian, Chinese, Thai, Momos",
        "average_cost_for_two": 1500,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/4/52294/55a3be58d549773c81b230501f9f88ce.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.9",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "2661"
        },
        "photos_url": "https://www.zomato.com/bangalore/asia-kitchen-by-mainland-china-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/asia-kitchen-by-mainland-china-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/4/52294/55a3be58d549773c81b230501f9f88ce.jpg?output-format=webp",
        "medio_provider": 1,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18221544",
        "order_url": "https://www.zomato.com/bangalore/asia-kitchen-by-mainland-china-koramangala-5th-block/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/asia-kitchen-by-mainland-china-koramangala-5th-block/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/asia-kitchen-by-mainland-china-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18323630
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18323630",
        "name": "House Of Commons",
        "url": "https://www.zomato.com/bangalore/house-of-commons-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "122/B, Jyothi Nivas Road, 5th Block, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9347174539",
          "longitude": "77.6157977432",
          "zipcode": "560095",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, Asian, North Indian",
        "average_cost_for_two": 1000,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/0/18323630/283c2a072c79e4b27c9d13ecf8591505.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.6",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "4812"
        },
        "photos_url": "https://www.zomato.com/bangalore/house-of-commons-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/house-of-commons-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/0/18323630/283c2a072c79e4b27c9d13ecf8591505.jpg",
        "medio_provider": 1,
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18323630",
        "is_table_reservation_supported": 1,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/bangalore/house-of-commons-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 57355
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "57355",
        "name": "The Boozy Griffin",
        "url": "https://www.zomato.com/bangalore/the-boozy-griffin-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "2nd Floor, 1st A Cross Road, Jyothi Nivas College Road, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9344779323",
          "longitude": "77.6162104681",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "European, Continental",
        "average_cost_for_two": 1800,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "zomato_events": [
          {
            "event": {
              "event_id": 65160,
              "friendly_start_date": "09 January",
              "friendly_end_date": "31 July",
              "friendly_timing_str": "Wednesday, 9th January - Wednesday, 31st July",
              "start_date": "2019-01-09",
              "end_date": "2019-07-31",
              "end_time": "19:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2016-04-11 12:32:51",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/eb4/1235359d66c0701354d4dc51711bceb4_1510061406.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/eb4/1235359d66c0701354d4dc51711bceb4_1510061406.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "1235359d66c0701354d4dc51711bceb4",
                    "id": 242913,
                    "photo_id": 242913,
                    "uuid": 1510061451473375,
                    "type": "NORMAL"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/eaf/fc5d6427966ad3aa7b72721aff8d7eaf_1556443656.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/eaf/fc5d6427966ad3aa7b72721aff8d7eaf_1556443656.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 1,
                    "md5sum": "fc5d6427966ad3aa7b72721aff8d7eaf",
                    "id": 463751,
                    "photo_id": 463751,
                    "uuid": 1556443515048117,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "MRP ! MRP ! MRP ! HAPPY HOURS",
              "description": "Every EVEN NUMBER OF YOUR DRINK IS ON MRP..\n* MONDAY - TUESDAY ENTIRE DAY \n* WEDNESDAY TO SUNDAY TILL 7PM \n* T&C APPLY",
              "display_time": "12:00 pm - 07:00 pm",
              "display_date": "09 January - 31 July",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Happy Hours",
                  "color": "#E23744"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 235371,
              "friendly_start_date": "09 January",
              "friendly_end_date": "31 July",
              "friendly_timing_str": "Wednesday, 9th January - Wednesday, 31st July",
              "start_date": "2019-01-09",
              "end_date": "2019-07-31",
              "end_time": "19:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2018-07-12 20:38:50",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/5d4/e07419b09d5cf8c2088bab6972a555d4_1555334186.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/5d4/e07419b09d5cf8c2088bab6972a555d4_1555334186.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "e07419b09d5cf8c2088bab6972a555d4",
                    "id": 457853,
                    "photo_id": 457853,
                    "uuid": 341732740,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "1+1 ON FRESHLY BREWED BEER",
              "description": "MONDAY - TUESDAY = FULL DAY \r\nWEDNESDAY TO SUNDAY TILL 7PM\r\n*T&C APPLY ",
              "display_time": "12:00 pm - 07:00 pm",
              "display_date": "09 January - 31 July",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 160768,
              "friendly_start_date": "09 January",
              "friendly_end_date": "31 July",
              "friendly_timing_str": "Wednesday, 9th January - Wednesday, 31st July",
              "start_date": "2019-01-09",
              "end_date": "2019-07-31",
              "end_time": "01:00:00",
              "start_time": "20:00:00",
              "is_active": 1,
              "date_added": "2017-10-07 15:39:21",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/96d/ec7143caff4b8e13bd6d5d3ecb6bb96d_1507370961.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/96d/ec7143caff4b8e13bd6d5d3ecb6bb96d_1507370961.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "ec7143caff4b8e13bd6d5d3ecb6bb96d",
                    "id": 234592,
                    "photo_id": 234592,
                    "uuid": 70870042868,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "BOLLYWOOD KARAOKE & LADIES NIGHT",
              "description": "FREE UNLIMITED COCKTAILS & SHOOTERS FOR LADIES EVERY WEDNESDAY 8pm ONWARDS BOLLYWOOD KARAOKE  FEAT. KJ PRITHVI @THE BOOZY GRIFFIN KORAMANGALA ,CALL: 9035308000",
              "display_time": "08:00 pm - 01:00 am",
              "display_date": "09 January - 31 July",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Karaoke",
                  "color": "#6454B8"
                },
                {
                  "name": "Ladies Night",
                  "color": "#0AAC9B"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 183192,
              "friendly_start_date": "11 January",
              "friendly_end_date": "26 July",
              "friendly_timing_str": "Friday, 11th January - Friday, 26th July",
              "start_date": "2019-01-11",
              "end_date": "2019-07-26",
              "end_time": "01:00:00",
              "start_time": "20:00:00",
              "is_active": 1,
              "date_added": "2018-01-03 12:43:49",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/db5/6d33da8e19e05bb974c99ebe2e596db5_1521657740.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/db5/6d33da8e19e05bb974c99ebe2e596db5_1521657740.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "6d33da8e19e05bb974c99ebe2e596db5",
                    "id": 287736,
                    "photo_id": 287736,
                    "uuid": 1521657705642845,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "GLOW IN THE DARK WITH DJ ",
              "description": "DRESS IN WHITE, FLORSCENT COLOURS & GET SOME NEON PROPS.",
              "display_time": "08:00 pm - 01:00 am",
              "display_date": "11 January - 26 July",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 100766,
              "friendly_start_date": "12 January",
              "friendly_end_date": "27 July",
              "friendly_timing_str": "Saturday, 12th January - Saturday, 27th July",
              "start_date": "2019-01-12",
              "end_date": "2019-07-27",
              "end_time": "01:00:00",
              "start_time": "20:00:00",
              "is_active": 1,
              "date_added": "2017-01-14 16:40:03",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/a20/379e431802ca7fe41a55cc9ab1ec3a20_1521657815.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/a20/379e431802ca7fe41a55cc9ab1ec3a20_1521657815.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "379e431802ca7fe41a55cc9ab1ec3a20",
                    "id": 287737,
                    "photo_id": 287737,
                    "uuid": 1521657763008286,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "BOLLYWOOD V/s HOLLYWOOD NIGHT with DJ Sada",
              "description": "DJ SPINNING THE HOTTEST HIP-HOP | COMMERCIAL | BOLLYWOOD",
              "display_time": "08:00 pm - 01:00 am",
              "display_date": "12 January - 27 July",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Live DJ",
                  "color": "#6454B8"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 348974,
              "friendly_start_date": "09 May",
              "friendly_end_date": "30 June",
              "friendly_timing_str": "Thursday, 9th May - Sunday, 30th June",
              "start_date": "2019-05-09",
              "end_date": "2019-06-30",
              "end_time": "22:00:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-05-09 16:40:57",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/eed/df714b7b808ccdb0983975e7441a9eed_1557400257.jpeg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/eed/df714b7b808ccdb0983975e7441a9eed_1557400257.jpeg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "df714b7b808ccdb0983975e7441a9eed",
                    "id": 470809,
                    "photo_id": 470809,
                    "uuid": 1557400253675659,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Click Share and Win!",
              "description": "Selfie Contest!",
              "display_time": "12:00 pm - 10:00 pm",
              "display_date": "09 May - 30 June",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Promos",
                  "color": "#F96C6A"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          }
        ],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/5/57355/27fa38bcb2db1fe9ad79e932d11c1aa9.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.6",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "5471"
        },
        "photos_url": "https://www.zomato.com/bangalore/the-boozy-griffin-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/the-boozy-griffin-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/5/57355/27fa38bcb2db1fe9ad79e932d11c1aa9.jpg?output-format=webp",
        "medio_provider": 1,
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/57355",
        "is_table_reservation_supported": 1,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/bangalore/the-boozy-griffin-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18586207
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18586207",
        "name": "The Terrace at Gilly's Redefined",
        "url": "https://www.zomato.com/bangalore/the-terrace-at-gillys-redefined-koramangala-4th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "413, 100 Feet Road, Next E-Zone, Koramangala 4th Block, Bangalore",
          "locality": "Gilly's Redefined, Koramangala 4th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9335329144",
          "longitude": "77.6234715432",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Gilly's Redefined, Koramangala 4th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, Finger Food, Asian, North Indian",
        "average_cost_for_two": 1400,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/7/18586207/630c252b6c79ee1d5be8e875303ecf49.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.5",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "1193"
        },
        "photos_url": "https://www.zomato.com/bangalore/the-terrace-at-gillys-redefined-koramangala-4th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/the-terrace-at-gillys-redefined-koramangala-4th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/7/18586207/630c252b6c79ee1d5be8e875303ecf49.jpg?output-format=webp",
        "medio_provider": 1,
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18586207",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/the-terrace-at-gillys-redefined-koramangala-4th-block/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/the-terrace-at-gillys-redefined-koramangala-4th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 54503
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "54503",
        "name": "Prost Brew Pub",
        "url": "https://www.zomato.com/bangalore/prost-brew-pub-koramangala-4th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "749, 10th Main, 80 Feet Road, Koramangala 4th Block, Bangalore",
          "locality": "Koramangala 4th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9332149667",
          "longitude": "77.6307839155",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Koramangala 4th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "American, Continental, North Indian, Salad",
        "average_cost_for_two": 1800,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 1,
        "mezzo_provider": "ZOMATO_BOOK",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/chains/3/54503/40171007914a46c536376e7bdb780e58.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.5",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "8238"
        },
        "photos_url": "https://www.zomato.com/bangalore/prost-brew-pub-koramangala-4th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/prost-brew-pub-koramangala-4th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/chains/3/54503/40171007914a46c536376e7bdb780e58.jpg",
        "medio_provider": "",
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/54503",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/prost-brew-pub-koramangala-4th-block/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/prost-brew-pub-koramangala-4th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 54044
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "54044",
        "name": "The Hole in the Wall Cafe",
        "url": "https://www.zomato.com/bangalore/the-hole-in-the-wall-cafe-koramangala-4th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "4, 8th Main Road, Koramangala 4th Block, Bangalore",
          "locality": "Koramangala 4th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9345668134",
          "longitude": "77.6256136224",
          "zipcode": "560034",
          "country_id": 1,
          "locality_verbose": "Koramangala 4th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Cafe, American, Burger",
        "average_cost_for_two": 600,
        "price_range": 2,
        "currency": "Rs.",
        "offers": [],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/res_imagery/54044_RESTAURANT_b3087c04bc909708516e2406a3679b72_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.6",
          "rating_text": "Excellent",
          "rating_color": "3F7E00",
          "votes": "7324"
        },
        "photos_url": "https://www.zomato.com/bangalore/the-hole-in-the-wall-cafe-koramangala-4th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/the-hole-in-the-wall-cafe-koramangala-4th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/res_imagery/54044_RESTAURANT_b3087c04bc909708516e2406a3679b72_c.jpg",
        "has_online_delivery": 0,
        "is_delivering_now": 0,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/54044",
        "is_table_reservation_supported": 0,
        "has_table_booking": 0,
        "events_url": "https://www.zomato.com/bangalore/the-hole-in-the-wall-cafe-koramangala-4th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    },
    {
      "restaurant": {
        "R": {
          "res_id": 18604249
        },
        "apikey": "6789e9fac008e779a9f047e6da9e754e",
        "id": "18604249",
        "name": "Crawl Street",
        "url": "https://www.zomato.com/bangalore/crawl-street-koramangala-5th-block?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "location": {
          "address": "77/A, 4th Floor Floor, Jyoti Nivas College Road, Koramangala 5th Block, Bangalore",
          "locality": "Koramangala 5th Block",
          "city": "Bangalore",
          "city_id": 4,
          "latitude": "12.9325490071",
          "longitude": "77.6139899343",
          "zipcode": "",
          "country_id": 1,
          "locality_verbose": "Koramangala 5th Block, Bangalore"
        },
        "switch_to_order_menu": 0,
        "cuisines": "Continental, Finger Food, North Indian, Chinese",
        "average_cost_for_two": 1200,
        "price_range": 3,
        "currency": "Rs.",
        "offers": [],
        "zomato_events": [
          {
            "event": {
              "event_id": 342533,
              "friendly_start_date": "24 April",
              "friendly_end_date": "31 December",
              "friendly_timing_str": "Wednesday, 24th April - Tuesday, 31st December",
              "start_date": "2019-04-24",
              "end_date": "2019-12-31",
              "end_time": "18:00:10",
              "start_time": "12:00:10",
              "is_active": 1,
              "date_added": "2019-04-24 14:23:58",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/66e/b234e0714672ccd074b628334ef3f66e_1556096118.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/66e/b234e0714672ccd074b628334ef3f66e_1556096118.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "b234e0714672ccd074b628334ef3f66e",
                    "id": 462127,
                    "photo_id": 462127,
                    "uuid": 15560961174108,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Happy Hours",
              "description": "Enjoy our happy hours with 1+1 on Spirits and wine, 2+1 on Tequila. Also on premium whiskey and beer, buy first at menu price and second on MRP!",
              "display_time": "12:00 pm - 06:00 pm",
              "display_date": "24 April - 31 December",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [
                {
                  "name": "Happy Hours",
                  "color": "#E23744"
                }
              ],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 350553,
              "friendly_start_date": "14 May",
              "friendly_end_date": "31 August",
              "friendly_timing_str": "Tuesday, 14th May - Saturday, 31st August",
              "start_date": "2019-05-14",
              "end_date": "2019-08-31",
              "end_time": "00:45:00",
              "start_time": "12:00:00",
              "is_active": 1,
              "date_added": "2019-05-13 18:09:02",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/62a/fb9e4f06a2fc549bdbd32cf891ea562a_1557751142.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/62a/fb9e4f06a2fc549bdbd32cf891ea562a_1557751142.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "fb9e4f06a2fc549bdbd32cf891ea562a",
                    "id": 473042,
                    "photo_id": 473042,
                    "uuid": 1557750910252963,
                    "type": "NORMAL"
                  }
                },
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/ef5/47574d5259425429f3a1f77c4d0f5ef5_1557751142.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/ef5/47574d5259425429f3a1f77c4d0f5ef5_1557751142.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 1,
                    "md5sum": "47574d5259425429f3a1f77c4d0f5ef5",
                    "id": 473043,
                    "photo_id": 473043,
                    "uuid": 1557750915330837,
                    "type": "NORMAL"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "ALL DAY MRP TUESDAY",
              "description": "Crawl Street presents weeks CHEAPEST DAY! A deal that definitely doesnt burn your pocket.\r\nEnjoy our entire liquor menu at MRP rates only on Tuesday, that too ENTIRE DAY!",
              "display_time": "12:00 pm - 12:45 am",
              "display_date": "14 May - 31 August",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 0,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          },
          {
            "event": {
              "event_id": 351079,
              "friendly_start_date": "19 May",
              "friendly_end_date": "19 May",
              "friendly_timing_str": "Sunday, 19th May",
              "start_date": "2019-05-19",
              "end_date": "2019-05-19",
              "end_time": "22:00:52",
              "start_time": "20:00:52",
              "is_active": 1,
              "date_added": "2019-05-14 16:44:23",
              "photos": [
                {
                  "photo": {
                    "url": "https://b.zmtcdn.com/data/zomato_events/photos/9bc/59508992c0484dc88f8a3f3a4a12e9bc_1557832479.jpg",
                    "thumb_url": "https://b.zmtcdn.com/data/zomato_events/photos/9bc/59508992c0484dc88f8a3f3a4a12e9bc_1557832479.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A",
                    "order": 0,
                    "md5sum": "59508992c0484dc88f8a3f3a4a12e9bc",
                    "id": 473670,
                    "photo_id": 473670,
                    "uuid": 15578324794108,
                    "type": "FEATURED"
                  }
                }
              ],
              "restaurants": [],
              "is_valid": 1,
              "share_url": "http://www.zoma.to/r/0",
              "show_share_url": 0,
              "title": "Sunday Live Band",
              "description": "Groove to the tunes of Midsummer Maya on Sunday 8pm onwards only at Crawl Street\nFrom London Thumakda to Shape of You! :)",
              "display_time": "08:00 pm - 10:00 pm",
              "display_date": "19 May",
              "is_end_time_set": 1,
              "disclaimer": "Restaurants are solely responsible for the service; availability and quality of the events including all or any cancellations/ modifications/ complaints.",
              "event_category": 1,
              "event_category_name": "",
              "book_link": "",
              "types": [],
              "share_data": {
                "should_show": 0
              }
            }
          }
        ],
        "opentable_support": 0,
        "is_zomato_book_res": 0,
        "mezzo_provider": "OTHER",
        "is_book_form_web_view": 0,
        "book_form_web_view_url": "",
        "book_again_url": "",
        "thumb": "https://b.zmtcdn.com/data/pictures/9/18604249/300863e5331cbf06643d91ebae11dce5.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A",
        "user_rating": {
          "aggregate_rating": "4.3",
          "rating_text": "Very Good",
          "rating_color": "5BA829",
          "votes": "2906"
        },
        "photos_url": "https://www.zomato.com/bangalore/crawl-street-koramangala-5th-block/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop",
        "menu_url": "https://www.zomato.com/bangalore/crawl-street-koramangala-5th-block/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop",
        "featured_image": "https://b.zmtcdn.com/data/pictures/9/18604249/300863e5331cbf06643d91ebae11dce5.jpg",
        "medio_provider": 1,
        "has_online_delivery": 1,
        "is_delivering_now": 1,
        "include_bogo_offers": true,
        "deeplink": "zomato://restaurant/18604249",
        "order_url": "https://www.zomato.com/bangalore/crawl-street-koramangala-5th-block/order?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "order_deeplink": "",
        "is_table_reservation_supported": 1,
        "has_table_booking": 1,
        "book_url": "https://www.zomato.com/bangalore/crawl-street-koramangala-5th-block/book?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "events_url": "https://www.zomato.com/bangalore/crawl-street-koramangala-5th-block/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1",
        "establishment_types": []
      }
    }
  ]
}`

const locationData = `{
  "entity_type": "group",
  "entity_id": "661",
  "title": "Bangalore Central, JP Nagar, Bengaluru",
  "latitude": "12.932020",
  "longitude": "77.625497",
  "city_id": "4",
  "city_name": "Bengaluru",
  "country_id": "1",
  "country_name": "India" 
}

`
export default mapData;

// "entity_type": "group",
//             "entity_id": 661,
//             "title": "Bangalore Central, JP Nagar, Bengaluru",
//             "latitude": 12.9161562991,
//             "longitude": 77.5924004987,
//             "city_id": 4,
//             "city_name": "Bengaluru",
//             "country_id": 1,
//             "country_name": "India"
