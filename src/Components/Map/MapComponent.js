import React, { PureComponent } from "react";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import config from "../../config";


let googleMapURL = `https://maps.googleapis.com/maps/api/js?key=${config.gmapKey}&v=3.exp&libraries=geometry,drawing,places`
export default class MapComponent extends PureComponent {
  constructor (props) {
    super (props);
    this.state = {

    }
  }

  render () {
    let locationData = this.props.locationData;
    let centerLocation = locationData && locationData.title ? locationData.title : locationData.location_suggestions[0];

    let restaurantsInLocation = this.props.restaurantsInLocation.restaurants;
    const MyMapComponent = withScriptjs(withGoogleMap((props) =>
      <GoogleMap
        defaultZoom={15}
        defaultCenter={{ lat: parseFloat(centerLocation.latitude), lng: parseFloat(centerLocation.longitude) }}
      >
        {props.isMarkerShown && <Marker position={{ lat: parseFloat(centerLocation.latitude), lng: parseFloat(centerLocation.longitude) }} label={centerLocation.title} />}

        {props.isMarkerShown && restaurantsInLocation && restaurantsInLocation.map((elm, i) => {
          return(
            <Marker 
              key={i} 
              position={{ lat: parseFloat(elm.restaurant.location.latitude), lng: parseFloat(elm.restaurant.location.longitude)}} 
              icon={{
                url: "icons/store.svg",
              }}
            />

          )
        })}
      </GoogleMap>
    ))
     return (
      <div>
      
        <MyMapComponent
          isMarkerShown
          googleMapURL={googleMapURL}
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div className="map-container-element" />}
          mapElement={<div style={{ height: `100%` }} />}
        />
      </div>
     )
  }
}
