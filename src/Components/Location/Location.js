import React, { PureComponent } from "react";
import MapComponent from "../Map/MapComponent"
import ApiClient from "./API";
import Spinner from "../Loader/Spinner.js";
import "../../css/location.css";

export default class Location extends PureComponent {
  constructor (props) {
    super (props);
    this.state = {
      isLoaded: false,
      locationInfo: {}
    }
    this.client = ApiClient();
    this.getLocationDetails = this.getLocationDetails.bind(this);
    
  }
  componentDidMount () {
    let locationName = this.props.location
    this.getLocationDetails(locationName);
  }

  getLocationDetails (locationName) {
    this.client.getRestaurentByRegion(locationName).then((json) => {
      this.setState({
        locationInfo: json.location,
        isLoaded: true,
      }, () => {
        this.props.checkRenderChild()
      })
    })
  }

  render () {
    let locationData = this.state.locationInfo.locationData;
    let restaurantsInLocation = this.state.locationInfo.restaurantsInLocation;
    return (
      <div className="map-container">
        {
          this.state.isLoaded ? (
            <MapComponent 
              restaurantsInLocation={restaurantsInLocation}
              locationData={locationData}
            />
          ) : <Spinner />
        }
        

        
      </div>
    )
  }
}

