export default function apiClient(token) {
  return {

    getRestaurentByRegion: function(location) {
      let url = 'api/getRestaurantByLocation';
      let paramObj = {
        locationName: location,
      }
      var urlParameters = getAsUriParameters(paramObj);
      return fetch(url + '?' + urlParameters, {
        credentials: 'include',
        mode: 'cors',
        method: 'GET',
        // body: body,

      }).then((response) => {
        if (response.ok) {
          return response.json();
          
        }
        throw new Error("Error");
      });
    },
  };
}



function getAsUriParameters(data) {
   var url = '';
   for (var prop in data) {
      url += encodeURIComponent(prop) + '=' + 
          encodeURIComponent(data[prop]) + '&';
   }
   return url.substring(0, url.length - 1)
}
