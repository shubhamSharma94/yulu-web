import React, { PureComponent, Fragment } from "react";
import Location from "./Location";
import ApiClient from "./API";
import ContentLoader from "../Loader/ContentLoader";

const LocationWithLoading = WithLoader(Location);

export default class LocationComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      locations: ["kormangala", "pawai", "magarphatta"],
      loadedTill: 0
    };
    this.client = ApiClient();
    this.checkRenderChild = this.checkRenderChild.bind(this);
  }


  checkRenderChild() {
    let loadedTill = this.state.loadedTill;
    this.setState({
      loadedTill: loadedTill + 1
    });
  }

  render() {
    return (
      <div>
        {
          <LocationWithLoading
            index={0}
            checkRenderChild={this.checkRenderChild}
            locations={this.state.locations}
            loadedTill={this.state.loadedTill}
            total={this.state.locations.length}
          />
        }
      </div>
    );
  }
}

function WithLoader(Component) {
  return ({ ...props }) => {
    return (
      <Fragment>
        <Component {...props} location={props.locations[0]} />
        {props.index >= props.total - 1 ? null : props.loadedTill >=
          props.index + 1 ? (
          <LocationWithLoading
            {...props}
            index={props.index + 1}
            locations={props.locations.slice(1)}
          />
        ) : (
          <ContentLoader />
        )}
      </Fragment>
    );
  };
}
