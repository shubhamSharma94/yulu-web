import React, { PureComponent } from "react";
import LocationComponent from "./Location/LocationComponent";


export default class App extends PureComponent {


  render () {
    return(
      <div>
        <LocationComponent />
      </div>
    )
  }
}